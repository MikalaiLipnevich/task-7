﻿using System;
using System.Text;

namespace Task6
{
    class Matrix<T>
    {
        private T[,] _data;

        public int Size { get; }
        public T this[int i, int j]
        {
            get { return _data[i, j]; }
            set
            {
                if (i < 0 || j < 0 || i > Size || j > Size)
                {
                    throw new MatrixException();
                }
                if (!_data[i, j].Equals(value))
                {
                    OnChangeElement(new ChangedElementEventArgs<T>(_data[i, j], value, i, j));
                    _data[i, j] = value;
                }

            }
        }
        public Matrix (int size)
        {
            if (size <= 0)
            {
                throw new MatrixException();
            }
            Size = size;
            _data = new T[Size, Size];
        }

        protected virtual void OnChangeElement (ChangedElementEventArgs<T> e)
        {
            Changed?.Invoke(this, e);
        }

        public event EventHandler<ChangedElementEventArgs<T>> Changed;

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    sb.AppendFormat("{0, 3} ", _data[i, j]);
                }
                sb.AppendLine();
            }
            string s = sb.ToString();
            return s;
        }
    }
}
