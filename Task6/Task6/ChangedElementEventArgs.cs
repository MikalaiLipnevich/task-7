﻿using System;

namespace Task6
{
    public class ChangedElementEventArgs<T> : EventArgs
    {
        public T OldValue { get; }
        public T NewValue { get; }
        public int IndexI { get; }
        public int IndexJ { get; }

        public ChangedElementEventArgs(T old, T @new, int indexi, int indexj)
        {
            OldValue = old;
            NewValue = @new;
            IndexI = indexi;
            IndexJ = indexj;
        }
    }
}
