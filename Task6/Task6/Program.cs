﻿using System;

namespace Task6
{
    class Program
    {

        public static void Show_Message<T>(object sender, ChangedElementEventArgs<T> e)
        {
            Console.WriteLine("Old = {0}, New = {1}, Indexes = {2},{3}", e.OldValue, e.NewValue, e.IndexI,e.IndexJ);
        }
        static void Main(string[] args)
        {

            var a = new IntMatrix(3);
            a[0, 0] = 1;
            a[0, 1] = 2;
            a[0, 2] = 3;
            a[1, 0] = 2;
            a[1, 1] = 3;
            a[1, 2] = 4;
            a[2, 0] = 3;
            a[2, 1] = 4;
            a[2, 2] = 5;

            var b = new IntMatrix(3);
            b[0, 0] = 3;
            b[0, 1] = 4;
            b[0, 2] = 5;
            b[1, 0] = 4;
            b[1, 1] = 5;
            b[1, 2] = 6;
            b[2, 0] = 6;
            b[2, 1] = 7;
            b[2, 2] = 8;

            var c = new IntMatrix(4);
            c = a.MultiplyMatrix(b);
            //var d = new IntMatrix(5);
            //d = c.MultiplyInt(-8);

            Console.WriteLine(c);
            Console.ReadKey();
            //a.Changed += Show_Message;


            //try
            //{
            //    var b = new Matrix<int>(0);
            //}
            //catch (MatrixException ex)
            //{
            //    Console.WriteLine(ex.Message);
            //    Console.ReadKey();
            //}

            a.Changed += (object sender, ChangedElementEventArgs<int> e) => Console.WriteLine("Old = {0}, New = {1}, Indexes = {2},{3}", e.OldValue, e.NewValue, e.IndexI, e.IndexJ);
            a[0, 1] = 5;
            Console.ReadKey();

            a.Changed += delegate (object sender, ChangedElementEventArgs<int> e)
            {
                Console.WriteLine("Old = {0}, New = {1}, Indexes = {2},{3}", e.OldValue, e.NewValue, e.IndexI, e.IndexJ);
            };

            a[0, 1] = 6;
            Console.ReadKey();
        }
    }
}
