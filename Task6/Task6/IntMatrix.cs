﻿using System;

namespace Task6
{
    class IntMatrix : Matrix<int>
    {
        public IntMatrix(int size) : base(size) { }

        public IntMatrix AddMatrix(IntMatrix matrix)
        {
            if (this.Size != matrix.Size)
            {
                throw new MatrixException();
            }
            IntMatrix newMatrix = new IntMatrix(Size);
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    newMatrix[i, j] = this[i, j] + matrix[i, j];
                }
            }
            return  newMatrix;
        }

        public IntMatrix MultiplyInt(int multiplier)
        {
            IntMatrix newMatrix = new IntMatrix(Size);
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    newMatrix[i, j] = this[i, j]*multiplier;
                }
            }
            return newMatrix;
        }

        public IntMatrix MultiplyMatrix(IntMatrix matrix)
        {
            if (this.Size != matrix.Size)
            {
                throw new MatrixException();
            }
            IntMatrix newMatrix = new IntMatrix(Size);
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    for (int k = 0; k < Size; k++)
                    {
                        newMatrix[i, j] += this[i, k] * matrix[k, j];
                    }   
                }
            }
            return newMatrix;
        }
    }
}
 