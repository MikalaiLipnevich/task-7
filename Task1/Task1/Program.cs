﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class FirstTask
    {
        static void Main(string[] args)
        {
            Console.Write("Input a: ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Input b: ");
            int b = int.Parse(Console.ReadLine());
            int c = 0;
            if (a > b)
            {
                int tmp = a;
                a = b;
                b = tmp;
            }

            while (a <= b)
            {

                if (a % 3 == 0 || a % 5 == 0 & a % 20 != 0)

                {

                    c += a;

                }
                a++;
            }
            Console.Write(c);
            Console.ReadKey();
        }
    }
}
