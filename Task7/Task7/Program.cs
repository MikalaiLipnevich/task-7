﻿using System;

namespace Task7
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var a = new SparseMatrix(5);
                a[0, 0] = 7;
                a[3, 3] = 2;
                a[4, 4] = 7;
                Console.WriteLine(a);
                Console.ReadKey();

                foreach (var i in a) Console.Write(i);
                Console.ReadKey();

                Console.WriteLine(a.Track());
                Console.ReadKey();

                Console.WriteLine(a.CheckCount(0));
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                Console.ReadKey();
            }
        }
    }
}