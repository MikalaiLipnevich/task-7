﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task7
{
    internal class SparseMatrix : IEnumerable<int>
    {
        public Dictionary<string, int> _matrix = new Dictionary<string, int>();

        public SparseMatrix(int size)
        {
            if (size <= 0) throw new Exception();
            Size = size;
        }

        public int Size { get; }

        public int this[int i, int j]
        {
            get
            {
                if (i < 0 || j < 0 || i >= Size || j >= Size) throw new Exception();
                var s = string.Format("{0},{1}", i, j);
                if (_matrix.ContainsKey(s)) return _matrix[s];
                return 0;
            }
            set
            {
                if (i < 0 || j < 0 || i >= Size || j >= Size) throw new Exception();
                var s = string.Format("{0},{1}", i, j);
                _matrix[s] = value;
            }
        }


        public IEnumerator<int> GetEnumerator()
        {
            return new SparseMatrixEnumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int[,] CreateMatrix()
        {
            var sb = new StringBuilder();
            var _temp = new int[Size, Size];
            foreach (var pair in _matrix)
            {
                var values = pair.Key.Split(',');
                var i = int.Parse(values[0]);
                var j = int.Parse(values[1]);
                _temp[i, j] = pair.Value;
            }

            return _temp;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++) sb.AppendFormat("{0, 3} ", CreateMatrix()[i, j]);
                sb.AppendLine();
            }

            var s1 = sb.ToString();
            return s1;
        }

        public int Track()
        {
            var _data = new int[Size];
            for (var i = 0; i < Size; i++)
            for (var j = 0; j < Size; j++)
                if (i == j)
                    _data[i] = CreateMatrix()[i, j];
            return _data.Sum();
        }

        public int CheckCount(int x)
        {
            return CreateMatrix().Cast<int>().ToArray().Count(p => p == x);
        }

        private class SparseMatrixEnumerator : IEnumerator<int>

        {
            private readonly int[] _data;
            private readonly SparseMatrix _sparseMatrix;
            private int _position = -1;

            public SparseMatrixEnumerator(SparseMatrix sparseMatrix)
            {
                _sparseMatrix = sparseMatrix;
                _data = new int[_sparseMatrix.Size];
                for (var i = 0; i < _sparseMatrix.Size; i++)
                for (var j = 0; j < _sparseMatrix.Size; j++)
                    if (i == j)
                        _data[i] = _sparseMatrix.CreateMatrix()[i, j];
            }

            public int Current => _data[_position];

            object IEnumerator.Current => _data[_position];

            public bool MoveNext()
            {
                if (_position < _data.Length - 1)
                {
                    _position++;
                    return true;
                }

                return false;
            }

            public void Reset()
            {
                _position = -1;
            }

            public void Dispose()
            {
            }
        }
    }
}