﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input number of elements: ");
            int a = int.Parse(Console.ReadLine());
            string[] data = new string[a];
            for (int i = 0; i < data.Length; i++)
            {
                Console.Write("Input element: ");
                data[i] = Console.ReadLine();
            }
            Console.WriteLine("Initial array: ");
            for (int i = 0; i < data.Length; i++)
            {
                Console.WriteLine(data[i]);
            }
            char[] vowel = { 'a', 'e', 'i', 'o', 'u', 'y' };
            int sum = 0;
            int[,] d = new int[a, 2];
            for (int i = 0; i < data.Length; i++)
            {
                sum = 0;
                for (int j = 0; j < data[i].Length; j++)
                {
                    for (int k = 0; k < vowel.Length; k++)
                    {
                        if (vowel[k] == data[i][j])
                            sum++;
                    }

                }
                d[i, 0] = sum;
                d[i, 1] = i;
            }

            for (int i = 0; i < a - 1; i++)
            {
                for (int j = i + 1; j < a; j++)
                {
                    if (d[i, 0] < d[j, 0])
                    {
                        int temp1 = d[i, 0];
                        int temp2 = d[i, 1];
                        d[i, 0] = d[j, 0];
                        d[i, 1] = d[j, 1];
                        d[j, 0] = temp1;
                        d[j, 1] = temp2;
                    }
                }
            }
            Console.WriteLine("Sorted array: ");
            for (int i = 0; i < a; i++)
            {
                if (d[i, 0] > 0)
                    Console.WriteLine(data[d[i, 1]]);
            }

            Console.ReadKey();
        }
    }
}
