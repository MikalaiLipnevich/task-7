﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public class Period
{
    private double _mass;
    public double Mass
    {
        get { return _mass; }
        set { _mass = value > 0 ? value : 0; }
    }
    public double CoordA { get; set; }
    public double CoordB { get; set; }
    public double CoordC { get; set; }
    private double[] _coord = new double[3];
    public double this[int i]
    {
        get {
            double x = _coord[i];
            switch (i)
            {
                case 0:
                    x = CoordA;
                    break;
                case 1:
                    x = CoordB;
                    break;
                case 2:
                    x = CoordC;
                    break;
            }
            return x;
            }
        set { _coord[i] = value > 0 ? value : 0;
            switch (i)
            {
                case 0:
                    CoordA = value;
                    break;
                case 1:
                    CoordB = value;
                    break;
                case 2:
                    CoordC = value;
                    break;
            }
        }
    }
    public Period(double coordA, double coordB, double coordC)
    {
        CoordA = coordA;
        CoordB = coordB;
        CoordC = coordC;
    }
    public Period(double coordA, double coordB, double coordC, double mass)
    {
        CoordA = coordA;
        CoordB = coordB;
        CoordC = coordC;
        Mass = mass > 0 ? mass : 0;
    }
    public bool IsZero()
    {
        if (CoordA == 0 && CoordB == 0 && CoordC == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public static double Distance (Period a, Period b)
    {
        double dis = 0;
        dis = Math.Sqrt(Math.Pow((a.CoordA - b.CoordA),2) + Math.Pow((a.CoordB - b.CoordB), 2) + Math.Pow((a.CoordC - b.CoordC), 2));
        return dis;
    }
}
namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Period p1 = new Period(0, 0, 0);
            Console.WriteLine(p1.IsZero());
            Console.ReadKey();

            Period p2 = new Period(4, 6, 8, -489498);

            p1[1] = 45.658;

            Console.WriteLine(p2[2]);
            Console.ReadKey();

            Console.WriteLine(Period.Distance(p1,p2));
            Console.ReadKey();

        }
    }
}

